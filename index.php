<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
//header_remove("X-Powered-By");
//header_remove("Server"); 
$c = new \Slim\Container;
$c['notFoundHandler'] = function ($c) {
	return function ($request, $response) use ($c) {
        return $c['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write('Page not found');
    };
};
$app = new \Slim\App($c);

function tokenCreate(){
	$secretKey=md5('123asdf');

	$jwt=array();
	$jwt['header']=base64_encode(json_encode(array(
		'typ'=>'JWT',
		'alg'=>'HS256')));
	$jwt['payload']=base64_encode(json_encode(array(
		'iss'=>'aaaa',
		'name'=>'a123',
		'email'=>'a123@abc.com',
		'admin'=>false,
		'exp'=>time()+3600)));
	$jwt['signature']=base64_encode(hash_hmac('sha256', implode('.', $jwt), $secretKey, true));

	return implode('.', $jwt);
}

$app->get('/',function(Request $request, Response $response){
	$token = $request->hasHeader('token');
	if(empty($token)){
		$response=$response->withHeader('Token',tokenCreate());
	}
	$data=array('method'=>'Home');
	return $response->withJson($data);
});

$app->get('/userProfile/{id}', function (Request $request, Response $response) {
	$id = $request->getAttribute('id');
	$data=array('method'=>'userProfile',
			   'id'=>$id);
	return $response->withJson($data);
});


$app->get('/activity[/[{page}]]', function (Request $request, Response $response) {
	$page = $request->getAttribute('page');
	$page=is_null($page)?0:$page;
	$data=array('method'=>'activity',
				'page'=>$page);
	return $response->withJson($data);
});


$app->get('/hello[/[{name}]]', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});
$app->run();